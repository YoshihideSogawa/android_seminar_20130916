package com.team_egg.android_seminar_20130916;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.TextView;

import com.google.example.games.basegameutils.BaseGameActivity;

/**
 * ゲームの画面です。
 * 
 * @author yoshihide-sogawa
 * 
 */
public class MainGameFragment extends Fragment {

    // 問題の全体管理
    /** 問題 */
    private int[] mQuestions;
    /** 問題数 */
    private int mQuestionSize;
    /** 現在の問題 */
    private int mCurrentQuestion;

    // 問題表示
    /** 対象のX倍返し */
    private int mTargetNum;
    /** 現在のX倍返し */
    private int mCurrentNum;

    /** {@link BaseGameActivity} */
    private MainContentsActivity mActivity;

    // View
    /** オーバレイ */
    private TextView mOverlayText;
    /** {@link Chronometer} */
    private Chronometer mChronometer;
    /** 残り問題数 */
    private TextView mRemainQuestionView;
    /** 対象のX倍返しのView */
    private TextView mTargetNumView;
    /** 現在のX倍返しのView */
    private TextView mCurrentNumView;
    /** 2倍返しボタン */
    private View mButtonX2;
    /** 3倍返しボタン */
    private View mButtonX3;
    /** 5倍返しボタン */
    private View mButtonX5;
    /** 7倍返しボタン */
    private View mButtonX7;
    /** リセット */
    private View mButtonReset;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        mActivity = (MainContentsActivity) activity;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View baseView = inflater.inflate(R.layout.fragment_main_game, null);
        // 問題の設定
        mQuestions = getResources().getIntArray(R.array.questions);
        mQuestionSize = mQuestions.length;
        // オーバレイ
        mOverlayText = (TextView) baseView.findViewById(R.id.overlay_text);
        // 時間
        mChronometer = (Chronometer) baseView.findViewById(R.id.chronometer);
        // 問題表示
        mRemainQuestionView = (TextView) baseView.findViewById(R.id.remain_question);
        mTargetNumView = (TextView) baseView.findViewById(R.id.target_num);
        mCurrentNumView = (TextView) baseView.findViewById(R.id.current_num);
        // ボタン
        mButtonX2 = baseView.findViewById(R.id.button_x2);
        mButtonX3 = baseView.findViewById(R.id.button_x3);
        mButtonX5 = baseView.findViewById(R.id.button_x5);
        mButtonX7 = baseView.findViewById(R.id.button_x7);
        mButtonReset = baseView.findViewById(R.id.button_reset);

        // 2倍返し
        mButtonX2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateCurrentNum(mCurrentNum * 2);
                if (checkAnswer()) {
                    startQuestion(mCurrentQuestion + 1);
                }
            }
        });

        // 3倍返し
        mButtonX3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateCurrentNum(mCurrentNum * 3);
                if (checkAnswer()) {
                    startQuestion(mCurrentQuestion + 1);
                }
            }
        });

        // 5倍返し
        mButtonX5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateCurrentNum(mCurrentNum * 5);
                if (checkAnswer()) {
                    startQuestion(mCurrentQuestion + 1);
                }
            }
        });

        // 7倍返し
        mButtonX7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateCurrentNum(mCurrentNum * 7);
                if (checkAnswer()) {
                    startQuestion(mCurrentQuestion + 1);
                }
            }
        });

        mButtonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateCurrentNum(1);
            }
        });

        return baseView;
    }

    /**
     * 初期化を行います。
     */
    void init() {
        startQuestion(0);
    }

    /**
     * 問題を開始します。
     * 
     * @param newQuestion
     */
    private void startQuestion(final int newQuestion) {
        // オーバレイ表示
        if (newQuestion == 0) {
            mOverlayText.setText(R.string.lets_begin);
            mOverlayText.setVisibility(View.VISIBLE);
        } else {
            mOverlayText.setText(R.string.correct);
            mOverlayText.setVisibility(View.VISIBLE);
            handleUnlockAchievement();
        }

        // 最終問題に到達していない場合
        if (newQuestion < mQuestionSize) {
            // 問題設定
            mCurrentQuestion = newQuestion;
            mNextQuestionDelay.start();
        }
        // 最終問題に到達している場合
        else {
            // 時間を止める
            mChronometer.stop();
            // 結果画面に結果を渡す
            final long elapsedTime = SystemClock.elapsedRealtime() - mChronometer.getBase();
            mActivity.startResult(elapsedTime);
        }
    }

    /**
     * 対象の値を更新します。
     * 
     * @param num
     *            何倍返しかの値
     */
    private void updateTargetNum(final int num) {
        mTargetNum = num;
        mTargetNumView.setText(getString(R.string.game_message, mTargetNum));
    }

    /**
     * 現在の値を更新します。
     * 
     * @param num
     *            何倍返しかの値
     */
    private void updateCurrentNum(final int num) {
        mCurrentNum = num;
        mCurrentNumView.setText(getString(R.string.game_message, mCurrentNum));
    }

    /**
     * X倍返しのチェックを行います。
     * 
     * @return X倍返し
     */
    private boolean checkAnswer() {
        if (mCurrentNum == mTargetNum) {
            return true;
        }

        return false;
    }

    /**
     * 実績のアンロックを処理します。
     */
    private void handleUnlockAchievement() {
        switch (mCurrentNum) {
        case 6:
            mActivity.unlockAchievement(getString(R.string.achievement_6));
            break;
        case 42:
            mActivity.unlockAchievement(getString(R.string.achievement_42));
            break;
        case 245:
            mActivity.unlockAchievement(getString(R.string.achievement_245));
            break;
        case 1024:
            mActivity.unlockAchievement(getString(R.string.achievement_1024));
            break;
        case 2205:
            mActivity.unlockAchievement(getString(R.string.achievement_2205));
            break;
        }
    }

    /** 次の問題へのディレイ */
    private final CountDownTimer mNextQuestionDelay = new CountDownTimer(2000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            // 処理なし
        }

        @Override
        public void onFinish() {
            updateTargetNum(mQuestions[mCurrentQuestion]);
            updateCurrentNum(1);
            mRemainQuestionView.setText(getString(R.string.remain_question, mCurrentQuestion + 1, mQuestionSize));
            if (mCurrentQuestion == 0) {
                // 時間再開
                mChronometer.setBase(SystemClock.elapsedRealtime());
                mChronometer.start();
            }
            // エフェクト非表示
            mOverlayText.setVisibility(View.GONE);
        }
    };

}
