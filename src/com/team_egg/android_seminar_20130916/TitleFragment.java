package com.team_egg.android_seminar_20130916;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.example.games.basegameutils.BaseGameActivity;

/**
 * タイトルの画面を表すフラグメントです。
 * 
 * @author yoshihide-sogawa
 * 
 */
public class TitleFragment extends Fragment {

    /** {@link BaseGameActivity} */
    private MainContentsActivity mActivity;

    /** ログインボタン */
    private View mSignInButton;
    /** ログアウトボタン */
    private View mSignOutButton;
    /** リーダボードボタン */
    private View mLeaderboardButton;
    /** アチーブメントボタン */
    private View mAchievementButton;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        mActivity = (MainContentsActivity) activity;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View baseView = inflater.inflate(R.layout.fragment_title, null);
        // スタートボタン
        baseView.findViewById(R.id.game_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.startMainGame();
            }
        });
        // ログインボタン
        mSignInButton = baseView.findViewById(R.id.sign_in_button);
        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.handleSignIn();
            }
        });
        // ログアウトボタン
        mSignOutButton = baseView.findViewById(R.id.sign_out_button);
        mSignOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.handleSignOut();
            }
        });
        // リーダボード
        mLeaderboardButton = baseView.findViewById(R.id.leaderboard);
        mLeaderboardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.startLeaderboard();
            }
        });
        // アチーブメントボタン
        mAchievementButton = baseView.findViewById(R.id.achievement);
        mAchievementButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.startAchievement();
            }
        });
        return baseView;
    }

    /**
     * Viewの状態を更新します。
     * 
     * @param isSignIn
     *            サインインしているかどうか
     */
    void updateView(final boolean isSignIn) {
        if (isSignIn) {
            mSignInButton.setVisibility(View.GONE);
            mSignOutButton.setVisibility(View.VISIBLE);
            mLeaderboardButton.setVisibility(View.VISIBLE);
            mAchievementButton.setVisibility(View.VISIBLE);
        } else {
            mSignInButton.setVisibility(View.VISIBLE);
            mSignOutButton.setVisibility(View.GONE);
            mLeaderboardButton.setVisibility(View.GONE);
            mAchievementButton.setVisibility(View.GONE);

        }
    }

}
