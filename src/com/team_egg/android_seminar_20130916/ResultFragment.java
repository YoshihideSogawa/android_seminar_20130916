package com.team_egg.android_seminar_20130916;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * 結果画面です。
 * 
 * @author yoshihide-sogawa
 * 
 */
public class ResultFragment extends Fragment {

    private MainContentsActivity mActivity;

    /** 結果メッセージ */
    private TextView mResultMessageView;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        mActivity = (MainContentsActivity) activity;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View baseView = inflater.inflate(R.layout.fragment_result, null);
        // 結果メッセージ
        mResultMessageView = (TextView) baseView.findViewById(R.id.result_message);
        // タイトルに戻るボタン
        baseView.findViewById(R.id.return_title).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.startTitle();
            }
        });
        return baseView;
    }

    /**
     * スコアの更新を行います。
     * 
     * @param finishTime
     *            クリア時間
     */
    void updateScore(final long finishTime) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss", Locale.getDefault());
        mResultMessageView.setText(getString(R.string.result_message, simpleDateFormat.format(new Date(finishTime))));
    }
}
