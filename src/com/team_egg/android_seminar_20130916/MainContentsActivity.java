package com.team_egg.android_seminar_20130916;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.google.example.games.basegameutils.BaseGameActivity;

/**
 * このアプリのメインとなる画面です。
 * 
 * @author yoshihide-sogawa
 * 
 */
public class MainContentsActivity extends BaseGameActivity {

    /** 使用しないリクエストコード */
    private static final int REQUEST_CODE_UNUSED = 5001;

    /** {@link TitleFragment} */
    private TitleFragment mTitleFragment;
    /** {@link MainGameFragment} */
    private MainGameFragment mMainGameFragment;
    /** {@link ResultFragment} */
    private ResultFragment mResultFragment;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_contents);

        final FragmentManager fm = getSupportFragmentManager();
        mTitleFragment = (TitleFragment) fm.findFragmentById(R.id.fragment_title);
        mMainGameFragment = (MainGameFragment) fm.findFragmentById(R.id.fragment_main_game);
        mResultFragment = (ResultFragment) fm.findFragmentById(R.id.fragment_result);
        startTitle();
    }

    /**
     * タイトル画面を表示します。
     */
    void startTitle() {
        final FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.show(mTitleFragment);
        ft.hide(mMainGameFragment);
        ft.hide(mResultFragment);
        ft.commitAllowingStateLoss();
    }

    /**
     * ゲーム画面を表示します。
     */
    void startMainGame() {
        mMainGameFragment.init();
        final FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.hide(mTitleFragment);
        ft.show(mMainGameFragment);
        ft.hide(mResultFragment);
        ft.commitAllowingStateLoss();
    }

    /**
     * 結果画面を表示します。
     * 
     * @param finishTime
     *            クリア時間
     */
    void startResult(final long finishTime) {
        mResultFragment.updateScore(finishTime);
        submitScore(getString(R.string.leaderboard_), finishTime);
        incrementAchievement(getString(R.string.achievement_clear_3), 1);
        final FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.hide(mTitleFragment);
        ft.hide(mMainGameFragment);
        ft.show(mResultFragment);
        ft.commitAllowingStateLoss();
    }

    /**
     * サインインを行います。
     */
    void handleSignIn() {
        beginUserInitiatedSignIn();
    }

    /**
     * サインアウトを行います。
     */
    void handleSignOut() {
        signOut();
        mTitleFragment.updateView(false);
    }

    /**
     * リーダボードを表示します。
     */
    void startLeaderboard() {
        // 全てのリーダボードを表示
        final Intent intent = getGamesClient().getAllLeaderboardsIntent();
        startActivityForResult(intent, REQUEST_CODE_UNUSED);
    }

    /**
     * 実績を表示します。
     */
    void startAchievement() {
        // 実績を表示
        final Intent intent = getGamesClient().getAchievementsIntent();
        startActivityForResult(intent, REQUEST_CODE_UNUSED);
    }

    /**
     * Achievementをアンロックします。
     * 
     * @param id
     *            実績ID
     */
    void unlockAchievement(final String id) {
        if (isSignedIn()) {
            getGamesClient().unlockAchievement(id);
        }
    }

    /**
     * Achievementを増加させます。
     * 
     * @param id
     *            実績ID
     * @param numSteps
     *            ステップ
     */
    void incrementAchievement(final String id, final int numSteps) {
        if (isSignedIn()) {
            getGamesClient().incrementAchievement(id, numSteps);
        }
    }

    /**
     * スコアを登録します。
     * 
     * @param id
     *            リーダボードID
     * @param scoreMillis
     *            ミリ秒のスコア
     */
    void submitScore(final String id, final long scoreMillis) {
        if (isSignedIn()) {
            getGamesClient().submitScore(id, scoreMillis);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSignInSucceeded() {
        mTitleFragment.updateView(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSignInFailed() {
        mTitleFragment.updateView(false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBackPressed() {
        // タイトル画面の場合
        if (mTitleFragment.isVisible()) {
            finish();
        }
        // メインゲームの場合
        else if (mMainGameFragment.isVisible()) {
            startTitle();
        }
        // 結果画面の場合
        else if (mResultFragment.isVisible()) {
            startTitle();
        }
    }

}
